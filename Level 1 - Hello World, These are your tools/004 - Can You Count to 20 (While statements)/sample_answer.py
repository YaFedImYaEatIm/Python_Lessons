#As creating a checker for this is not suitable, a sample answer is provided instead.
count = 0
goal = 20
while count < goal:
    print(count + 1)
    count += 1