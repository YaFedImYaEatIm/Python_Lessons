#As creating a checker for this is not suitable, a sample answer is provided instead.
#and statement
print("AND STATEMENT EXAMPLE")
for i in range(0, 20):
    if i % 2 == 0 and i % 3 == 1:
        print(i)
        
print("\nOR STATEMENT EXAMPLE")
#or statment
for i in range(0, 20):
    if i < 5 or i >= 15:
        print(i)
        
print("\nNOT STATEMENT EXAMPLE")        
#not statement
for i in range(0, 20):
    if i % 2 == 0 and not i == 2:
        print(i)