#As creating a checker for this is not suitable, a sample answer is provided instead.
num_of_even_numbers = 10

#solution 1
num_of_numbers_printed = 0
counter = 2
while (num_of_numbers_printed < num_of_even_numbers):
    print(counter)
    counter += 2
    
#or alternative
for i in range(0, num_of_even_numbers * 2, 2):
    print(i)
    
#or
for i in range(0, num_of_even_numbers):
    print(i * 2 + 2)
    
#or
counter = 2
while (num_of_numbers_printed < num_of_even_numbers):
    if counter % 2 == 0:
        print(counter)
    counter += 1
    
#or many other different solutions, programming has many solutions to a single problem, some, more practical than others.